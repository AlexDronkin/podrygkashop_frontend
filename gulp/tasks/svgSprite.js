const gulp = require('gulp')
const svgstore = require('gulp-svgstore')
const rename = require('gulp-rename')
const replace = require('gulp-replace')

module.exports = function svgSprite() {
  return gulp.src('src/svg/*.svg')
    .pipe(replace("&gt;", ">"))
    .pipe(svgstore({
      inlineSvg: true
    }))
    .pipe(rename('sprite.svg'))
    .pipe(gulp.dest('build/img'))
}

