/**
 * @link https://osvaldas.info/caching-svg-sprite-in-localstorage
 */

import svg4everybody from '../local_modules/svg4everybody/dist/svg4everybody.min'
import $ from '../local_modules/jquery/dist/jquery.min'

$(document).ready(() => {

  var file = 'img/sprite.svg',
      revision = 1;

  if( !document.createElementNS || !document.createElementNS( 'http://www.w3.org/2000/svg', 'svg' ).createSVGRect )
    return true;

  var isLocalStorage = 'localStorage' in window && window[ 'localStorage' ] !== null,
    request,
    data,
    insertIT = function()
    {
      document.body.insertAdjacentHTML( 'afterbegin', '<div style="display: none">'+data+'</div>' );
    },
    insert = function()
    {
      if( document.body ) insertIT();
      else document.addEventListener( 'DOMContentLoaded', insertIT );
    };

  if( isLocalStorage && localStorage.getItem( 'inlineSVGrev' ) == revision )
  {
    data = localStorage.getItem( 'inlineSVGdata' );
    if( data )
    {
      insert();
      return true;
    }
  }

  try
  {
    request = new XMLHttpRequest();
    request.open( 'GET', file, true );
    request.onload = function()
    {
      if( request.status >= 200 && request.status < 400 )
      {
        data = request.responseText;
        insert();
        if( isLocalStorage )
        {
          localStorage.setItem( 'inlineSVGdata',  data );
          localStorage.setItem( 'inlineSVGrev',   revision );
        }
      }
    }
    request.send();
  }
  catch( e ){}

  svg4everybody()


})
