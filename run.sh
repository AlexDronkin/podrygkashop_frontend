#!/bin/sh

case "$1" in

-u)
  docker stop podrygkashop_frontend
  docker run \
    -it \
    --rm \
    --name podrygkashop_frontend \
    -v "$PWD":/usr/src/app \
    podrygkashop/frontend:1 \
    npm install
  ;;

-b)
  docker rmi podrygkashop/frontend:1
  docker build -t podrygkashop/frontend:1 .

  docker run \
    -it \
    --rm \
    --name podrygkashop_frontend \
    -v "$PWD":/usr/src/app \
    podrygkashop/frontend:1 \
    npm install
  ;;

*)
  docker stop podrygkashop_frontend
  docker run \
    -it \
    --rm \
    --name podrygkashop_frontend \
    -v "$PWD":/usr/src/app \
    -p 88:3000 \
    podrygkashop/frontend:1 \
    npm run start
  ;;
esac
